//code generate automatic not edit date: 2023-11-24T01:32:10.533Z
import dotenv from "dotenv";
import Subscribe from "onbbu/subscribe";

import bvtmnzyxajtndtc from "@api/bvtmnzyxajtndtc";

dotenv.config();

const subscribe: Subscribe = new Subscribe();

subscribe.use("bvtmnzyxajtndtc::destroy", async (instance: { id: number}) => {
  await bvtmnzyxajtndtc.destroy({where: { id:[instance.id] } } );
});


const start = async (): Promise<void> => subscribe.run();

const stop = async (): Promise<void> => subscribe.stop();

export default { start, stop };