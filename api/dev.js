//code generate automatic not edit date: 2023-11-25T02:15:05.174Z
const { raceFunction } = require("onbbu")
const api = require("./dist")

const data = [
	{
		name: "product01",
		description: "desc product01",
		precio: 120,
		stock: 100,
		category: "cat01",
	},
	{
		name: "product02",
		description: "desc product02",
		precio: 120,
		stock: 100,
		category: "cat02",
	},

]

async function create() {

	const instance = await api.create(data[1])

	console.log(instance)
}

async function update() {

	const instance = await api.update({where:{id:2},params:{name:"product02", description:"producto02"}})

	console.log(instance)
}

async function destroy() {

	const instance = await api.destroy({where:{id:[3]}})

	console.log(instance)
}

async function findAndCount() {

	const instance = await api.findAndCount({where:{id:[1,2,3,4], category:["cat01","cat02"]}, paginate:{offset:0,limit:5}})

	console.log(instance.data)
}

async function findOne() {

	const instance = await api.findOne({where:{id:3}})

	console.log(instance)
}

async function main() {

	try {

		// await raceFunction(create)

		// await raceFunction(update)

		// await raceFunction(destroy)

		await raceFunction(findAndCount)

		// await raceFunction(findOne)

	} catch (error) {
		console.log(error)
	}
}

raceFunction(main, 250)