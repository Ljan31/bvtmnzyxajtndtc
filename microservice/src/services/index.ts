//code generate automatic not edit date: 2023-11-24T01:31:29.673Z
import model from "../models";
import * as T from "../types";
import { NotFoundError, Paginate, Response } from "onbbu";

export const create = async (params: T.Create): Response<Partial<T.ModelAttributes>> => {
	
  const data:Partial<T.ModelAttributes> = await model.create(params);

  return { statusCode:"success", data: data };
};

export const update = async ({ where, params }: T.Update): Response<Partial<T.ModelAttributes>> => {

  const {affected, instances } = await model.update(params, {where});

  if(affected === 0) {throw new NotFoundError("not found error update");}

  return { statusCode: "success", data: instances[0]};
};

export const destroy = async ({ where }: T.Destroy): Response<Partial<T.ModelAttributes>[]> => {

  const { affected, instances  } = await model.destroy({where});

  if(affected === 0) {throw new NotFoundError("not found error destroy");}

  return { statusCode: "success", data: instances };
};

export const findAndCount = async ({ where, paginate }: T.FindAndCount): Response<Paginate<Partial<T.ModelAttributes>>> => {

  const {data, itemCount, pageCount} = await model.findAndCountAll({where, ...paginate});

  if(data.length === 0) {return { statusCode:"success", data: {data:[], itemCount:0, pageCount:0} };}

  return { statusCode:"success", data: {data, itemCount, pageCount} };
};

export const findOne = async ({ where }: T.FindOne): Response<Partial<T.ModelAttributes>> => {

  const data:Partial<T.ModelAttributes> | null = await model.findOne({where});

  if (data === undefined) {throw new NotFoundError("not found instance");}

  return { statusCode:"success", data: data };
};