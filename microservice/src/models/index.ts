//code generate automatic not edit date: 2023-11-24T01:32:10.510Z
import Models, { DataTypes, S } from "onbbu/models";
import * as T from "../types";

const model: Models<Partial<T.ModelAttributes>> = new Models<Partial<T.ModelAttributes>>(T.name);

model.define({

  attributes: {

    name: DataTypes.STRING,

    description: DataTypes.STRING,

    precio: DataTypes.BIGINT,

    stock: DataTypes.BIGINT,

    category: DataTypes.STRING,

  },
  options: { freezeTableName: true }
});

export const sync = (props: S.SyncOptions) => model.sync(props);

export default model;