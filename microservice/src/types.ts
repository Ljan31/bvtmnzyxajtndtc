//code generate automatic not edit date: 2023-11-24T01:28:12.000Z
//@onbbu-name: micro product
export const name = "bvtmnzyxajtndtc";

export interface ModelAttributes {
	id: number

	name: string
	description: string
	precio: number
	stock: number
	category: string
	createdAt: string
	updatedAt: string
}

export interface Create {
	name: string
	description: string
	precio: number
	stock: number
	category: string
}

export interface Update {
	where: {
		id: number
	}
	params: {
		name?: string
		description?: string
		precio?: number
		stock?: number
		category?: string
	}
}

export interface Destroy {
	where: {
		id: number[]
	}
}

export interface FindAndCount {
	where: {
		id?: number[]
		category?: string[]
	}
	paginate: {
		offset?: number
		limit?: number
	}
}

export interface FindOne {
	where: {
		id?: number
	}
}



export const endpoint = [
  "create",
  "update",
  "destroy",
  "findAndCount",
  "findOne",
] as const;

export type Endpoint = typeof endpoint[number]