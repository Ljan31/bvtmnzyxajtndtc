# microservicio producto
```
export interface ModelAttributes {
	id: number

	name: string
	description: string
	precio: number
	stock: number
	category: string
	createdAt: string
	updatedAt: string
}
```